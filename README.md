# Swift2Gnuplot

_Swift2Gnuplot uses Swift with the Homebrew flavor of `gnuplot` which is available on macOS, Linux and Windows 10 Subsystem for Linux._

## Getting Started

1. Install `gnuplot` from using [Homebrew](https://brew.sh/).

    ``` sh
    brew install gnuplot
    ```
    
2. Download or clone this `Swift2Gnuplot` package.
3. Context-click on `Package.swift` and select open in Xcode.
4. Optionally, edit [Sources/Swift2Gnuplot/main.swift](Sources/Swift2Gnuplot/main.swift) to include/exclude particular examples.
5. Run. 

The Swift2Gnuplot package can also be run from the terminal command line.

``` sh
cd $PATH_TO_PACKAGE/Swift2Gnuplot
swift run
```

> _Note: Tests are not yet implemented._

<!--
## Initialization

Gnuplot default setting are read from `gnuplotrc` which is located in the Homebrew Cellar. For example, /usr/local/Cellar/gnuplot/5.4.2/share/gnuplot/5.4/gnuplotrc on Intel macOS.

A `.gnuplot` file can be added to the user's home director for custom initialization defaults on macOS and Linux.  The `gnuplotrc` can be copied to `.gnuplot` as a baseline. (See docs for Windows gnuplot.ini file information.)

``` sh
### File: .gnuplot
set term qt persist size 700,500 font 'Arial,12'
```

Some initialization can also be done in the terminal initialization files e.g. `.zshrc`, `.profile` or `.bash_profile`.

``` sh
################
### GNU PLOT ###
################
export GNUTERM=qt
```
-->

## Pipe Interface

> _Note: Gnuplot script command output goes to the `stderr` output. Gnuplot plotting products can be directed to the `stdout` output instead of a file or a terminal window._

The `gnuplot` commands are communicated through the stdin, stdout and stderr process pipe interface via the Swift Foundation `Process` interface. Swift2Gnuplot was developed on macOS Big Sur, and should likely be able to run on Linux.

For iOS, some gnuplot source code changes would need to made for the pipe interface. See ["gnuplot with iOS"](https://stackoverflow.com/questions/8088657/gnuplot-with-ios) question and answer on StackOverflow. It's possible, since [iCAS](https://apps.apple.com/us/app/icas-workstation-class-scientific/id394637176#?platform=ipad) uses [gnuplot in their iPhone/iPad appliction](https://stackoverflow.com/questions/8088657/gnuplot-with-ios/12014386#12014386).

## Syntax Highlights (Gnuplot)

**cd**

`cd '<directory-name>'`

**loadpath**

The loadpath setting defines additional locations for data and command files searched by the call, load, plot and splot commands. If a file cannot be found in the current directory, the directories in loadpath are tried.

``` sh
set loadpath {"pathlist1" {"pathlist2"...}}
show loadpath
```

Path names may be entered as single directory names, or as a list of path names separated by a platform- specific path separator, eg. colon (`:`) on Unix, semicolon (`;`) on Windows platforms. The `show loadpath`, `save` and `save set` commands replace the platform-specific separator with a space character (` `).

**pwd**

The pwd command prints the name of the working directory to the screen or stderr.

**Qt Terminal**

```
set term qt {<n>}
    {size <width>,<height>}
    {position <x>,<y>}
    {title "title"}
    {font <font>} {{no}enhanced}
    {linewidth <lw>} {dashlength <dl>}
    {{no}persist} {{no}raise} {{no}ctrl}
    {close}
    {widget <id>}
    
# <n> window number    
```

**User-Defined variables and functions**

``` sh
# string variable
file = "mydata.inp"
# string function
file(n) = sprintf("run_%d.dat",n)
```

## Versus Matplotlib

[Gnuplot-py vs Matplotlib](https://izziswift.com/gnuplot-vs-matplotlib/) from a Python programming perspective.

* common
    * mature software. significant user base.
    * large collection of technical 2D and 3D data graph representations.
    * common output formats include: EPS, PNG, PDF, Postscript, SVG 
* `gnuplot`
    * source language: C
    * Python integration wrappers include: [autogpy](https://pypi.org/project/autogpy/), [gnuplot-kernel](https://pypi.org/project/gnuplot-kernel/), [gnuplot-manager](https://pypi.org/project/gnuplot-manager/), [gnuplotlib](https://pypi.org/project/gnuplotlib/), [GnuplotPy3](https://pypi.org/project/GnuplotPy3/), [py-gnuplot](https://pypi.org/project/py-gnuplot/)
    * faster. some cases much faster.
    * plotting: command line and script driven. supports piping.
    * can achieve similar aesthetics by adding some extra instruction details
    * publication output: LaTeX
* `Matplotlib`
    * source language: Python
    * Python integration: better, easier since it's Python
    * better _default_ aesthetics 

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* [gnuplot.info](http://www.gnuplot.info/) (gnuplot homepage)
    * [demo page](http://gnuplot.info/screenshots/index.html#demos): [v5.4](http://gnuplot.sourceforge.net/demo_5.4/)
    * [demo source files](https://sourceforge.net/p/gnuplot/gnuplot-main/ci/master/tree/demo/)
* Homebrew `gnuplot`: [linux](https://formulae.brew.sh/formula-linux/gnuplot), [macos](https://formulae.brew.sh/formula/gnuplot)
* [iCAS](https://apps.apple.com/us/app/icas-workstation-class-scientific/id394637176#?platform=ipad): iPad/iPhone application which [uses gnuplot](). 

**Swift Docs**

* Language
    * [Swift Programming Language (online book)](https://docs.swift.org/swift-book/index.html)
* Foundation
    * [Pipe](https://developer.apple.com/documentation/foundation/pipe)
    * [Process](https://developer.apple.com/documentation/foundation/process)    
* Packages
    * [Algorithms](https://github.com/apple/swift-algorithms)
    * [Numerics](https://github.com/apple/swift-numerics)

**Other Homebrew Formulae Which Use Gnuplot**

* [feedgnuplot](https://formulae.brew.sh/formula/feedgnuplot) Tool to plot realtime and stored data from the command-line
* [matplotplusplus](https://formulae.brew.sh/formula-linux/matplotplusplus) C++ Graphics Library for Data Visualization ... a C++ interface for Gnuplot
* [maxima](https://formulae.brew.sh/formula/maxima) Computer algebra system
    * [wxmaxima](https://formulae.brew.sh/formula/wxmaxima) Cross platform GUI for Maxima {C++/wxWidgets}
* [octave](https://formulae.brew.sh/formula/octave) High-level interpreted language for numerical computing
* [Qalculate!](https://qalculate.github.io/index.html) [(github)](https://github.com/Qalculate) {C++}
    * [libqalculate](https://formulae.brew.sh/formula/libqalculate) library for [Qalculate!](https://qalculate.github.io/index.html) {GTK}
    * [qalculate-gtk](https://formulae.brew.sh/formula/qalculate-gtk)
* [siril](https://formulae.brew.sh/formula/siril) Astronomical image processing tool
* [tsung](https://formulae.brew.sh/formula/tsung) Load testing for HTTP, PostgreSQL, Jabber, and others

