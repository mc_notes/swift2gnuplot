// swift-tools-version:5.4
import PackageDescription

let package = Package(
    name: "Swift2Gnuplot",
    platforms: [
        .macOS(.v11), // minimum deployment requirement
    ],
    dependencies: [
        .package(url: "https://github.com/apple/swift-algorithms", from: "0.1.0"),
        .package(url: "https://github.com/apple/swift-numerics", from: "0.1.0"),
    ],
    targets: [
        .executableTarget(
            name: "Swift2Gnuplot",
            dependencies: [
                .product(name: "Algorithms", package: "swift-algorithms"),
                .product(name: "Numerics", package: "swift-numerics"),
            ]
        ),
        .testTarget(
            name: "Swift2GnuplotTests",
            dependencies: ["Swift2Gnuplot"]),
    ],
    swiftLanguageVersions: [SwiftVersion.v5],
    cLanguageStandard: CLanguageStandard.c11,
    cxxLanguageStandard: CXXLanguageStandard.cxx14
)
