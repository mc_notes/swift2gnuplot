//
//  GnuplotExample01.swift
//  Swift2Gnuplot
//

import Foundation

struct GnuplotExample01 {
    
    var title = "Example 01"

    func script() -> String {
        """
        set term qt persist size 700,500 font 'Arial,12'
        set title '\(title)'
        plot sin(x)
        quit\n
        """
    }
    
    func plot() {
        let argv: [String] = ["--persist"]
        let executable = McProcessPath.gnuplot
        
        let task = McProcess(
            executable: executable,
            arguments: argv,
            scriptIn: script()
        )
        
        let result = task.run()
        print(result) // expected result: (stdout: "", stderr: "") empty string
    }
    
}
