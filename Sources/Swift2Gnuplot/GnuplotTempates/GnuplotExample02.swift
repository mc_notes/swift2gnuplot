//
//  GnuplotExample02.swift
//  Swift2Gnuplot
//

import Foundation

/// Note: `GnuplotProtocol` provides the `plot()` method,
/// thus each (example) template can be more focused on the  `gnuplot` script.

/// `example_02_qt_protocol` sends sin(x) to the Qt terminal. Adds use of `McProcess` and `GnuplotProtocol`.
struct GnuplotExample02: GnuplotProtocol {
    
    var title = "Example 02"

    func script() -> String {
        """
        set term qt persist size 700,500 font 'Arial,12'
        # set output 'example.02.png'
        set key fixed left top vertical Right noreverse enhanced autotitle box lt black linewidth 1.000 dashtype solid
        set samples 50, 50
        set title "Example 02"
        set title  font ",20" textcolor lt -1 norotate
        set xrange [ * : * ] noreverse writeback
        set x2range [ * : * ] noreverse writeback
        set yrange [ * : * ] noreverse writeback
        set y2range [ * : * ] noreverse writeback
        set zrange [ * : * ] noreverse writeback
        set cbrange [ * : * ] noreverse writeback
        set rrange [ * : * ] noreverse writeback
        NO_ANIMATION = 1
        plot [-10:10] sin(x),atan(x),cos(atan(x))

        quit\n
        """
    }
    
}
