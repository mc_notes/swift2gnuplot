//
//  GnuplotExample00.swift
//  Swift2Gnuplot
//

import Foundation

/// Basic sin(x). No data. Does not use `McProcess`.

struct GnuplotExample00 {
    
    var filename = "FIND_ME.00.png"
    
    func script() -> String {
        """
        set terminal png
        set output '\(filename)'
        plot sin(x)
        quit\n
        """
    }
    
    func plot() {
        let argv: [String] = ["--persist"]
        let executable = McProcessPath.gnuplot
        
        let task = McProcess(
            executable: executable,
            arguments: argv,
            scriptIn: script()
        )
        
        let result = task.run()
        print(result) // expected result: (stdout: "", stderr: "") empty string
    }
    
}

/* ************************************
 set terminal png
 set output 'FIND_ME_01.png'
 plot sin(x)
 quit\n
 */

// gnuplot -e "set terminal png; set output '01.png'; plot sin(x); quit"
