//
//  GnuplotExample03.swift
//  Swift2Gnuplot
//

import Foundation

/// Note: `GnuplotProtocol` provides the `plot()` method,
/// thus each (example) template can be more focused on the  `gnuplot` script.

/// `example_03_qt_data` uses a .
struct GnuplotExample03: GnuplotProtocol {
    
    var title = "Example 03"
    var loadpath = McProcess.baseOutputDir.path
    var setname0 = "0.dat"
    var setname1 = "1.dat"
    var setname2 = "2.dat"

    func script() -> String {
        """
        #
        # Gnuplot version 5.0 demo of multiplot auto-layout capability
        #
        #
        set loadpath '\(loadpath)'
        set term qt persist position 0,0 size 600,1000 font 'Arial,12'
        
        set multiplot layout 3, 1 title "Multiplot Layout 3R x 1C" font ",12"
        set tmargin 3 # adjusted for multiple plot title

        # Plot: Upper
        set title "Plot 3.a Position"
        unset key
        plot [0:2*pi] '\(setname0)' with lines

        # Plot: Middle
        set title "Plot 3.b Velocity"
        unset key
        plot [0:2*pi] '\(setname1)'

        # Plot: Lower
        
        set key bmargin left horizontal Right noreverse enhanced autotitle box lt black linewidth 1.000 dashtype solid
        set samples 800, 800
        set title 'Plot 3.c Acceleration ... actually, all 3 datasets :-)'
        set title  font ",12" textcolor lt -1 norotate
        set xrange [ * : * ] noreverse writeback
        set x2range [ * : * ] noreverse writeback
        set yrange [ * : * ] noreverse writeback
        set y2range [ * : * ] noreverse writeback
        set zrange [ * : * ] noreverse writeback
        set cbrange [ * : * ] noreverse writeback
        set rrange [ * : * ] noreverse writeback
        NO_ANIMATION = 1

        plot [0:2*pi] '\(setname0)' with lines, '\(setname1)' ,'\(setname2)' with impulses

        quit\n
        """
    }
    
    ///
    mutating func addScatterData(points: [DataPoint], name: String, set: Int) {
        let filename = "\(name).dat"
        switch set {
        case 0:
            setname0 = filename
        case 1:
            setname1 = filename
        case 2:
            setname2 = filename
        default:
            print("ERROR: addScatterData() set number must be in range 0 ..< 2")
            return
        }
        
        let dataUrl = McProcess.baseOutputDir
            .appendingPathComponent(filename, isDirectory: false)
        var dataText = ""
        for p in points {
            dataText.append("\(p.x) \(p.y)\n")
        }
        do {
            try dataText.write(to: dataUrl, atomically: true, encoding: .utf8)
        } catch {
            print("Error: addScatterData '\(filename)' while writing \(error)")
        }
    }
    
}
