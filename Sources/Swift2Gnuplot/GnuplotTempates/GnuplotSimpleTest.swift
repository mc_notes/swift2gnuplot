//
//  GnuplotSimpleTest.swift
//  Swift2Gnuplot
//

import Foundation

/// Basic sin(x). No data. Does not use `McProcess`.
/// https://stackoverflow.com/questions/28925874/calling-gnuplot-in-swift

struct GnuplotSimpleTest {
    
    func script() -> String {
        """
        set terminal png
        set output 'FIND_ME_TEST.png'
        plot sin(x)
        quit\n
        """
    }
    
    func plot() {
        var argv: [String] = []
        argv.append("--persist")            // do not close interactive plot windows on exit
        //argv.append("--default-settings") // ignore `gnuplotrc`, `.gnuplot` initialization
        //argv.append("--slow")             // wait for slow font initialization on startup
        // -e  "command1; command2; ..."
        // -c  scriptfile ARG1 ARG2 ...
        
        let task = Process()
        task.arguments = argv
        //                "/opt/homebrew/bin/gnuplot" // Big Sur, Apple Silicon
        //                "/usr/local/bin/gnuplot"    // Big Sur, Intel
        task.executableURL = McProcessPath.gnuplot
        
        let workingDir = McProcess.baseOutputDir
        task.currentDirectoryURL = workingDir // where to find the output
            
        let pipeIn = Pipe()
        task.standardInput = pipeIn
        let pipeOut = Pipe()
        task.standardOutput = pipeOut
        let pipeErr = Pipe()
        task.standardError = pipeErr
        
        do {
            try task.run()
            let commandData = script().data(using: .utf8)!
            pipeIn.fileHandleForWriting.write(commandData)
            
            let dataOut = pipeOut.fileHandleForReading.readDataToEndOfFile()
            if let output = String(data: dataOut, encoding: .utf8) {
                print("STANDARD OUTPUT\n" + output)
            }

            let dataErr = pipeErr.fileHandleForReading.readDataToEndOfFile()
            if let outputErr = String(data: dataErr, encoding: .utf8) {
                print("STANDARD ERROR \n" + outputErr)
            }
        } catch {
            print("FAILED: \(error)")
        }
        task.waitUntilExit()
        print("STATUS: \(task.terminationStatus)")
    }
    
}

