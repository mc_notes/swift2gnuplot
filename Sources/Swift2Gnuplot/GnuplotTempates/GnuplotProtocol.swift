//
//  GnuplotProtocol.swift
//  Swift2Gnuplot
//

import Foundation

protocol GnuplotProtocol {
    func script() -> String
}

extension GnuplotProtocol {
    
    func plot() {
        let argv: [String] = ["--persist"]
        let executable = McProcessPath.gnuplot
        
        let task = McProcess(
            executable: executable,
            arguments: argv,
            scriptIn: script()
        )
        
        let result = task.run()
        print("stdout:\n\(result.stdout)")
        print("stderr:\n\(result.stderr)")
    }
    
}
