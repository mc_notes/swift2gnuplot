//
// main.swift
// Swift2Gnuplot
//

import Foundation
//
import Algorithms // https://github.com/apple/swift-algorithms
import Numerics // https://github.com/apple/swift-numerics

// ----- comment/uncomment to select example(s) to run   -----

Examples.example_00_png()
Examples.example_01_qt_mcprocess()
Examples.example_02_qt_protocol()

// ----- your main notebook here   -----
// Note: the level of code shown here has a global scope.

struct DataPoint {
    var x: Double
    var y: Double
}

var position = [DataPoint]()
var velocity = [DataPoint]()
var acceleration = [DataPoint]()

let pi = Double.pi // alternately: Float.pi, Float64.pi, Float32.pi

// plot 1 of 3
var xStep: Double = (2.0 * pi) / 100.0
var x = 0.0
while x < 2.0 * pi {
    position.append(DataPoint(x: x, y: sin(x)))
    x += xStep
}

// plot 2 of 3
x = 0.0
while x < 2.0 * pi {
    velocity.append(DataPoint(x: x, y: 0.5 * cos(x)))
    x += xStep
}

// plot 3 of 3
x = 0.0
while x < 2.0 * pi {
    acceleration.append(DataPoint(x: x, y: -0.25 * sin(x)))
    x += xStep
}

var example03 = GnuplotExample03()
example03.addScatterData(points: position, name: "Data.0.Position", set: 0)
example03.addScatterData(points: velocity, name: "Data.1.Velocity", set: 1)
example03.addScatterData(points: acceleration, name: "Data.2.Acceleration", set: 2)
example03.plot()

// That's all Folks!
