//
//  RuntimeInfo.swift
//  Swift2Gnuplot/Helpers
//
//

import Foundation

/// Note: `ProcessInfo` provides a superset of `CommandLine` features, so `ProcessInfo` is used here.
///
/// https://developer.apple.com/documentation/swift/commandline
/// https://developer.apple.com/documentation/foundation/processinfo

struct RuntimeInfo {
    
    static func printAll() {
        printRuntimePath()
        printArguments()
        printEnvironment()
        printShellEnvironment()
    }
    
    /// argv[0] contains the executable file path
    static func printArguments() {
        var s = """
        ############################
        ## RuntimeInfo: arguments ##\n
        """
        
        //let argv: [String] = CommandLine.arguments
        let argv: [String] = ProcessInfo.processInfo.arguments
        let argc: Int = argv.count
        let command: String = URL(fileURLWithPath: argv[0]).lastPathComponent
        s.append("command == \(command)\n")
        s.append("   argc == \(argc)\n")
        for i in 0 ..< argc {
            s.append("\(i): \(argv[i])\n")
        }
        print(s)
    }

    /// Variable keys:values in the environment from which the process was launched.
    static func printEnvironment() {
        var s = """
        ##############################
        ## RuntimeInfo: environment ##\n
        """

        for kv in ProcessInfo.processInfo.environment {
            s.append("\(kv.key)=\(kv.value)\n")
        }
        print(s)
    }
    
    /// Print runtime path
    static func printRuntimePath() {
        let argv: [String] = ProcessInfo.processInfo.arguments
        let runtimeUrl = URL(fileURLWithPath: argv[0]).deletingLastPathComponent()
        print(runtimeUrl.absoluteString)
    }
    
    /// Variable keys:values in the shell environment launched from this process.
    static func printShellEnvironment() {
        var s = """
        #################################################
        ## RuntimeInfo: /usr/bin/env shell environment ##\n
        """
        let process = McProcess(executable: McProcessPath.env)
        let result = process.run()
        s.append(result.stdout)
        
        print(s)
    }

    /// Return runtime path
    static func runtimePath() -> String {
        return runtimeUrl().absoluteString
    }

    /// Return runtime URL
    static func runtimeUrl() -> URL {
        let argv: [String] = ProcessInfo.processInfo.arguments
        let runtimeUrl = URL(fileURLWithPath: argv[0]).deletingLastPathComponent()
        return runtimeUrl
    }

}

// Note: On macOS, ProcessInfo.processInfo.environment includes DYLD_FRAMEWORK_PATH, DYLD_LIBRARY_PATH, LD_LIBRARY_PATH which is not otherwise present in `/usr/bin/env`.  Otherwise, the shell subprocess defaults to inherit the same key-values.
