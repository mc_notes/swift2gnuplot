//
//  McProcessPath.swift
//  MCxSystemExecOsax
//
//  Created by marc-medley on 2017.04.03.
//  Copyright © 2017-2021 marc-medley. All rights reserved.
//

import Foundation

public class McProcessPath {
        
    /// `curl`
    public static let curl = URL(fileURLWithPath: "/usr/bin/curl", isDirectory: false)

    /// `env`
    public static let env = URL(fileURLWithPath: "/usr/bin/env", isDirectory: false)

    /// `ffmpeg`
    public static let ffmpeg = URL(fileURLWithPath: "/usr/local/bin/ffmpeg", isDirectory: false)

    /// `ffprobe`
    public static let ffprobe = URL(fileURLWithPath: "/usr/local/bin/ffprobe", isDirectory: false)

    /// `gnuplot`
    /// /opt/homebrew/bin/gnuplot // Big Sur, Apple Silicon
    /// /usr/local/bin/gnuplot // Big Sur, Intel
    public static let gnuplot = URL(fileURLWithPath: "/usr/local/bin/gnuplot", isDirectory: false)

    /// GraphicsMagick `gm`
    public static let gm = URL(fileURLWithPath: "/usr/local/bin/gm", isDirectory: false)
    
    /// Inkscape `inkscape`
    // NOTE: simple `inkscape` link will not find Inkscape's Resources
    public static let inkscape = URL(
        fileURLWithPath: "/Applications/Inkscape.app/Contents/Resources/bin/inkscape", 
        isDirectory: false)

    /// `open`
    public static let open = URL(fileURLWithPath: "/usr/bin/open", isDirectory: false)
    
    /// `pandoc`
    public static let pandoc = URL(fileURLWithPath: "/usr/local/bin/pandoc", isDirectory: false)
    
    /// `tesseract` OCR
    public static let tesseract = URL(fileURLWithPath: "/usr/local/bin/tesseract", isDirectory: false)
    
    /// `textutil` 
    public static let textutil = URL(fileURLWithPath: "/usr/bin/textutil", isDirectory: false)

    /// `wget`
    public static let wget = URL(fileURLWithPath: "/usr/local/bin/wget", isDirectory: false)
        
}
