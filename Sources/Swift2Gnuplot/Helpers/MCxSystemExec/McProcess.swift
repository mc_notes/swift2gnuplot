//
//  McProcess.swift
//  MCxSystemExecOsax
//
//  Created by marc-medley on 2017.01.26.
//  Copyright © 2017-2021 marc-medley. All rights reserved.
//

import Foundation

/// https://developer.apple.com/documentation/foundation/process
/// https://developer.apple.com/documentation/foundation/pipe
/// 
/// ## Important:
/// 
/// `$HOME` and `~` must be programatically expanded prior to use.
/// 
/// ## NYI:
/// Add auto detection and expansion for `$HOME` and `~`.
/// 

struct McProcess {
    
    static var baseOutputDir: URL = {
        let fm = FileManager.default
        let url = fm.homeDirectoryForCurrentUser
            .appendingPathComponent("Desktop")
            .appendingPathComponent("GnuplotExampleIO")
        if fm.fileExists(atPath: url.path) == false {
            try? fm.createDirectory(
                at: url,
                withIntermediateDirectories: true,
                attributes: [:]) // [FileAttributeKey : Any]?
        }
        return url
    }()
        
    // settings
    var arguments: [String]?
    var workingDir: URL
    var scriptInData: Data?
    var executable: URL
    /// flag to enable/disable printing `stdout`, `stderr` results
    var printResults: Bool
    
    init(executable: URL,
         arguments: [String]? = nil,
         scriptIn: String? = nil,
         workingDir: URL? = nil,
         workingFolder: String? = nil, // appended to workingDir, if provided
         printResults: Bool = false) {
        
        self.arguments = arguments
        if let dir = workingDir {
            self.workingDir = dir
        } else {
            self.workingDir = McProcess.baseOutputDir
        }
        if let folder = workingFolder {
            // update: workingDir/workingFolder
            self.workingDir.appendPathComponent(folder, isDirectory: true)
        }
        if let script = scriptIn, let data = script.data(using: .utf8) {
            self.scriptInData = data
        }
        self.executable = executable
        self.printResults = printResults
    }
    
    func run() -> (stdout: String, stderr: String) {
        // https://developer.apple.com/documentation/foundation/process
        let task = Process()
        task.executableURL = executable
        if let argv = arguments {
            task.arguments = argv
        }
        task.currentDirectoryURL = workingDir
        
        let pipeIn = Pipe()
        task.standardInput = pipeIn
        let pipeOutput = Pipe()
        task.standardOutput = pipeOutput
        let pipeError = Pipe()
        task.standardError = pipeError
        
        do {
            try task.run()
            
            var stdoutStr = "" // does not mask foundation `stdout`
            var stderrStr = "" // does not mask foundation `stderr`
            
            if let stdinData = scriptInData {
                pipeIn.fileHandleForWriting.write(stdinData)
            }
            
            let data = pipeOutput.fileHandleForReading.readDataToEndOfFile()
            if let output = String(data: data, encoding: String.Encoding.utf8) {
                stdoutStr.append(output)
            }
            
            let dataError = pipeError.fileHandleForReading.readDataToEndOfFile()
            if let outputError = String(data: dataError, encoding: String.Encoding.utf8) {
                stderrStr.append(outputError)
            }
            
            task.waitUntilExit()
            if printResults {
                print("STANDARD OUTPUT\n" + stdoutStr)
                print("STANDARD ERROR \n" + stderrStr)
                print("STATUS: \(task.terminationStatus)")
            }
            
            return (stdoutStr, stderrStr)
        } catch {
            let errorStr = "FAILED: \(error)"
            return ("", errorStr)
        }
    }
    
    /// can be used in debugger `po printDescription()`
    func printDescription(includeScriptIn: Bool = true) {
        var s = ""
        s.append("   arguments: \(arguments ?? [])\n")
        s.append("  workingDir: \(workingDir.absoluteString)\n")
        if includeScriptIn, let dataIn = scriptInData {
            s.append("    scriptIn: \n\(String(data: dataIn, encoding: .utf8)!)\n")
        }
        s.append("  executable: \(executable.absoluteString)\n")
        s.append("printResults: \(printResults)\n")
        print(s)
    }
    
}
