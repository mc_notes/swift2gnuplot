//
//  McShell.swift
//  MCxSystemExecOsax
//
//  Created by marc-medley on 2017.03.27.
//  Copyright © 2017-2021 marc-medley. All rights reserved.
//

import Foundation

// 
public class McShell {
    
    /// shell: Graphics Magic convert resize
    /// 
    /// ```
    /// gm convert $ICON_SOURCE_IMAGE -resize 16x16     $ICON_NAME.iconset/icon_16x16.png
    /// ```
    public static func gmConvertResize(fromPath: String, toPath: String, wxh: String, workingDir: URL) {
        var argv: [String] = []
        argv.append("convert")
        argv.append(fromPath)
        argv.append("-resize")
        argv.append(wxh)
        argv.append(toPath)
        
        let process = McProcess(
            executable: McProcessPath.gm,
            arguments: argv,
            workingDir: workingDir
        )
        _ = process.run()
    }
    
    ///
    /// 
    /// inkscape 
    /// 
    /// - Note: If h x w ratio does not match the original ratio, then the resulting file will have stretch distortion.  
    ///
    /// _Will launch XQuartz._
    ///
    /// - parameters:
    ///     - fromPath: fully specified file path
    ///     - toPath: full specified file path
    ///     - h: pixel height
    ///     - w: pixel width
    ///     - backgroundOpacity: range 0.0 to 1.0. Default is 0.005 to preserve canvas size. 0.0 contracts to image size.
    ///     - workPath: current working directory
    ///
    public static func inkscapeSvgPng(fromPath: String, toPath: String, h: Int, w: Int, backgroundOpacity: Float = 0.005, workUrl: URL) {
        //print("--export-height=\(h)")
        //print("--export-width=\(w)")
        //print("--export-png='\(toPath)'")
        //print("--export-background-opacity=\(backgroundOpacity)")
        //print("fromPath = \(fromPath)")
        //print("workUrl = \(workUrl)")
        var argv: [String] = []
        argv.append("--without-gui")
        argv.append("--export-height=\(h)")
        argv.append("--export-width=\(w)")
        argv.append("--export-background-opacity=\(backgroundOpacity)")
        argv.append(contentsOf: ["--export-png=\(toPath)"])
        argv.append(fromPath)
        
        let process = McProcess(
            executable: McProcessPath.inkscape,
            arguments: argv,
            workingDir: workUrl
        )
        _ = process.run()
    }
        
    /// shell: open
    /// 
    /// ```
    /// open [-a application] [-b bundle_indentifier] [_MORE_] file ...
    /// ```
    public static func open(filePath: String, workingDir: URL? = nil) {
        var argv: [String] = []
        argv.append(filePath)
        
        let process = McProcess(
            executable: McProcessPath.open, 
            arguments: argv,
            workingDir: workingDir
        )
        _ = process.run()
    }
    
    
}
