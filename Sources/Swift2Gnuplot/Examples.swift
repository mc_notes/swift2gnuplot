//
//  Examples.swift
//  Swift2Gnuplot
//

import Foundation

/// Examples provides a container for various examples.
struct Examples {
    
    /// `example_00_png` outputs a sin(x) PNG file.
    static func example_00_png() {
        // -- comment/uncomment, as desired ---
        print("Hello, Swift2Gnuplot!")  // Just print hello :-)
        
        // -- comment/uncomment to show/hide runtime environment values ---
        //RuntimeInfo.printRuntimePath()
        //RuntimeInfo.printArguments()
        RuntimeInfo.printEnvironment()
        //RuntimeInfo.printShellEnvironment()
        
        // -- comment/uncomment baseline examples
        GnuplotSimpleTest().plot() // saves FIND_ME_00.png to Desktop
        GnuplotExample00().plot()  // default: saves FIND_ME_01.png to executable directory
        
        McShell.open(filePath: McProcess.baseOutputDir.path)
        //McShell.open(filePath: RuntimeInfo.runtimePath())
    }
    
    /// `example_01_qt_mcprocess` sends sin(x) to the Qt terminal. Adds use `McProcess`.
    static func example_01_qt_mcprocess() {
        GnuplotExample01().plot()
    }

    /// `example_02_qt_protocol` sends sin(x) to the Qt terminal. Adds use of `McProcess` and `GnuplotProtocol`.
    static func example_02_qt_protocol() {
        let example_02 = GnuplotExample02()
        example_02.plot()
    }

}
